# Comment installer et configurer une authentification OIDC pour l'UNIGE sous Linux

Cette documentation présente la marche à suivre pour configurer une authentification OIDC en utilisant un serveur Apache
HTTPD comme client OIDC qui joue le role de proxy devant une application/service à protéger.

D'autres clients OIDC peuvent également être utilisés pour mettre en place une authentification OIDC, pour autant que
ceux-ci respectent les standards OpenID Connect (https://openid.net/developers/specs/).

OIDC offre plusieurs méthodes de connexions appelées "flows". Ce document présente celui que nous utilisons généralement
pour l'authentification/autorisation d'une application Web. À savoir: **Authorization code flow**.

Pour plus d'informations sur ce
flow: https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow

## Installation du module Apache OpenIDC

La configuration présentée dans cette documentation nécessite d'utiliser une version supérieure >= 2.3.10 du module
mod_auth_openidc
(https://github.com/zmartzone/mod_auth_openidc). Cette version est nécessaire afin de pouvoir permettre l'utilisation de
l'option `OIDCRefreshAccessTokenBeforeExpiry` qui permet de laisser le module Apache gérer automatique le
rafraichissement des token. Sans cette option, il est nécessaire, au niveau de l'application web, de gérer la
récupération d'un nouveau token d'accès lorsque celui-ci est expiré.

Le binaire mod_auth_openidc peut être récupéré à cet
emplacement : https://github.com/zmartzone/mod_auth_openidc/releases
Dans la mesure du possible, installez la version la plus récente.

## Demander la configuration de l'IdP

[Soumettre une demande de déclaration de site pour la configuration SSO dans l'infrastructure UNIGE](https://support-si.unige.ch/openentry.html?tid=SRD000000004902)

## Configuration du module OIDC

Pour ajouter une authentification OIDC à une application ou autre service, un serveur Apache doit être monté en proxy
devant l'application. L'ensemble des requêtes HTTP doivent passer par le serveur Apache HTTPD.

La configuration est relativement simple, il suffit de renseigner diverses options du module mod_auth_openidc au niveau
d'un VirtualHost Apache configuré pour l'application que l'on souhaite sécuriser.

Le VirtualHost utilisé pour l'application doit contenir les configurations suivants :

### Pour l'environnement ISIS de production

```apacheconf
<VirtualHost *:443>
   ...

  <Location />
    ...
    AuthType openid-connect
    Require claim aud:[ADFS client ID]
  </Location>

	# In order to avoid calling userinfo_endpoint the OIDCProviderUserInfoEndpoint must not be set
	# For this reason, it's not possible to use the OIDCProviderMetadataURL option, every OIDCProvider
	# URL must be set manually to ensure the userinfo_endpoint is not set.
	# For more information see discussion : https://groups.google.com/g/mod_auth_openidc/c/Fw9dGXPXrNY/m/_YOn3fF-AQAJ
	OIDCProviderIssuer https://adfs.unige.ch/adfs
	OIDCProviderAuthorizationEndpoint https://adfs.unige.ch/adfs/oauth2/authorize/
	OIDCProviderJwksUri https://adfs.unige.ch/adfs/discovery/keys
	OIDCProviderTokenEndpoint https://adfs.unige.ch/adfs/oauth2/token/
	OIDCProviderEndSessionEndpoint https://adfs.unige.ch/adfs/oauth2/logout

	OIDCSessionInactivityTimeout 3600
	OIDCSessionMaxDuration 28800
	OIDCCryptoPassphrase [Crypto passphrase]
	OIDCClientID [ADFS client ID]
	OIDCClientSecret [ADFS client secret]
	OIDCRedirectURI [URL de redirection]
	OIDCScope "openid allatclaims"
	OIDCRefreshAccessTokenBeforeExpiry 60
	OIDCPassClaimsAs headers
	OIDCRemoteUserClaim upn

	<Location "/oidc-callback">
      AuthType openid-connect
      Require claim "upn~^[w+\S+]*@.*unige\.ch$"
    </Location>
</VirtualHost>
```

### Pour l'environnement ISIS de test

```apacheconf
<VirtualHost *:443>
   ...

  <Location />
    ...
    AuthType openid-connect
    Require claim aud:[ADFS client ID]
  </Location>

	# In order to avoid calling userinfo_endpoint the OIDCProviderUserInfoEndpoint must not be set
	# For this reason, it's not possible to use the OIDCProviderMetadataURL option, every OIDCProvider
	# URL must be set manually to ensure the userinfo_endpoint is not set.
	# For more information see discussion : https://groups.google.com/g/mod_auth_openidc/c/Fw9dGXPXrNY/m/_YOn3fF-AQAJ
	OIDCProviderIssuer https://adfsklif.unige.ch/adfs
	OIDCProviderAuthorizationEndpoint https://adfsklif.unige.ch/adfs/oauth2/authorize/
	OIDCProviderJwksUri https://adfsklif.unige.ch/adfs/discovery/keys
	OIDCProviderTokenEndpoint https://adfsklif.unige.ch/adfs/oauth2/token/
	OIDCProviderEndSessionEndpoint https://adfsklif.unige.ch/adfs/oauth2/logout

	OIDCSessionInactivityTimeout 3600
	OIDCSessionMaxDuration 28800
	OIDCCryptoPassphrase [Crypto passphrase]
	OIDCClientID [ADFS client ID]
	OIDCClientSecret [ADFS client secret]
	OIDCRedirectURI [URL de redirection]
	OIDCScope "openid allatclaims"
	OIDCRefreshAccessTokenBeforeExpiry 60
	OIDCPassClaimsAs headers
	OIDCRemoteUserClaim upn

	<Location "/oidc-callback">
      AuthType openid-connect
      Require claim "upn~^[w+\S+]*@.*unige\.ch$"
    </Location>
</VirtualHost>
```

Les placeholders suivants (indiqués entre crochets) doivent être renseignés:

- **[ADFS client ID]** = Client ID de l'application ADFS\
- **[ADFS client secret]** = Client secret de l'application ADFS\
- **[URL de redirection]** = URL vers laquelle le serveur d'autorisation (ADFS) redirigera sa requête, exemple
  d'URL: https://my-app.unige.ch/oidc-callback \
- **[Crypto passphrase]** = Chaine de caractères utilisée pour encrypter le "state cookie" et les entrées dans le cache.
  N'importe quelle chaine de caractères peut être utilisée. Nous recommandons toutefois d'en générer une en utilisant un
  hash sha-256. On peut générer un hash de manière aléatoire avec la commande bash suivante :
  ```bash
  dd if=/dev/urandom bs=32 count=1 2>/dev/null | sha256sum -b | sed 's/ .*//'
  ```
  Il est également possible de générer un hash en utilisant des sites
  comme: https://onlinehashtools.com/generate-random-sha256-hash

Dans cette configuration nous avons volontairement omis de renseigner
l'option `OIDCProviderMetadataURL https://adfs.unige.ch/adfs/.well-known/openid-configuration`
et avons spécifié manuellement les URLs de chaque enpoints nécessaires à l'authentification OIDC. Cette configuration
était nécessaire pour permettre au module mod_auth_openidc de ne pas envoyer d'appels au userinfo_endpoint pour
récupérer les données de l'utilisateur connecté (celles-ci étant déjà contenues dans l'access token transmis après une
authentification réussie). Pour plus d'informations sur cette problématique, voir échange
suivant: https://groups.google.com/g/mod_auth_openidc/c/Fw9dGXPXrNY/m/_YOn3fF-AQAJ

## Exemple

Deux exemples de configurations Apache, réalisées pour ajouter une authentification OIDC devant une application, sont
disponibles ici:

- [my-app.unige.ch-example.conf](my-app.unige.ch-example.conf (environnement ISIS))
- [my-app-test.unige.ch-example.conf](my-app.unige.ch-example_klif.conf (environnement ISIS de test))

## Documentation

La documentation des différentes options offertes par le module mod_auth_openidc se trouve
ici : https://github.com/zmartzone/mod_auth_openidc/blob/master/auth_openidc.conf

## Debug

En cas de problème de configuration, ajoutez la directive Apache `LogLevel Debug` dans votre VirtualHost. Cela permettra
d'afficher des erreurs détaillées provenant du module Apache OIDC.


